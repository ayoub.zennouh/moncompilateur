//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <stack>
using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum DATA_TYPE {INTEGER, CHAR, DOUBLE, BOOLEAN, UNKN};

TOKEN current;				// Current token

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, DATA_TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
DATA_TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	if(!IsDeclared(lexer->YYText()))
		Error("Variable n'est pac declaree");
	DATA_TYPE DataType=DeclaredVariables[lexer->YYText()]; 
	current=(TOKEN) lexer->yylex();
	return DataType;
}

DATA_TYPE Number(void){
	double d;
	unsigned int *i;
	string number=lexer->YYText();
	if(number.find(".")!=string::npos){
		d=atof(lexer->YYText());
		i=(unsigned int *) &d;
		cout <<"\tsubq $8,%rsp"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}
DATA_TYPE Character(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax"<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}
DATA_TYPE Boolean(void ){
	if(!strcmp(lexer->YYText(),"FALSE"))
		cout<<"\tpush $0\t#FALSE"<<endl;
	else
		cout<<"\tpush $0xFFFFFFFFFFFFFFFF\t#TRUE"<<endl;
	current=(TOKEN) lexer->yylex();
	return BOOLEAN;
}

DATA_TYPE Expression(void);			// Called by Term() and calls Term()

DATA_TYPE Factor(void){
	DATA_TYPE DataType=UNKN;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		DataType=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			DataType=Number();
	     	else
				if(current==ID)
					DataType=Identifier();
				else
					if(current==CHARACTER)
						DataType=Character();
					else
						if((current==KEYWORD)&&(!strcmp(lexer->YYText(),"FALSE")||!strcmp(lexer->YYText(),"TRUE")))
							DataType=Boolean();
						else
							Error("'(' ou chiffre ou lettre attendue");
	return DataType;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
DATA_TYPE Term(void){
	OPMUL mulop;
	DATA_TYPE DataType=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();
		if(Factor() != DataType)
			Error("type different");
		cout << "\tpop %rbx"<<endl;
		cout << "\tpop %rax"<<endl;
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;
				cout << "\tpush %rax\t# AND"<<endl;
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;
				cout << "\tpush %rax\t# MUL"<<endl;
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl;
				cout << "\tdiv %rbx"<<endl;
				cout << "\tpush %rax\t# DIV"<<endl;
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl;
				cout << "\tdiv %rbx"<<endl;
				cout << "\tpush %rdx\t# MOD"<<endl;
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return DataType;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
DATA_TYPE SimpleExpression(void){
	OPADD adop;
	DATA_TYPE DataType=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();
		if(Term() != DataType)
			Error("type different");
		cout << "\tpop %rbx"<<endl;
		cout << "\tpop %rax"<<endl;
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;
	}
	return DataType;
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclaration(void);
void VarDeclarationPart(void){
	if(current!=KEYWORD || strcmp("VAR",lexer->YYText()))
		Error("Mot cle 'VAR' attendu");
	do{
		VarDeclaration();
	}while(current==SEMICOLON);
	if(current!=DOT)
		Error("Carectere '.' attendu");
	current=(TOKEN) lexer->yylex();
}
// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	stack<string> VarName;
	do{
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("ID attendu");
		VarName.push(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}while(current==COMMA);
	if(current!=COLON)
		Error("caractere ':' attendu");
	current=(TOKEN) lexer->yylex();
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;	
	if(!strcmp(lexer->YYText(),"INTEGER"))
		while(!VarName.empty()){
			DeclaredVariables.insert(pair<string, DATA_TYPE>(VarName.top(), INTEGER)); 
			cout << VarName.top() <<":\t.quad 0"<<endl;
			VarName.pop();
		}
	else if(!strcmp(lexer->YYText(), "DOUBLE"))
		while(!VarName.empty()){
			DeclaredVariables.insert(pair<string, DATA_TYPE>(VarName.top(), DOUBLE));
			cout << VarName.top() <<":\t.byte 0"<<endl;
			VarName.pop();
		}
	else if(!strcmp(lexer->YYText(), "CHAR"))
		while(!VarName.empty()){
			DeclaredVariables.insert(pair<string, DATA_TYPE>(VarName.top(), CHAR));
			cout << VarName.top() << ":\t.double 0.0"<<endl;
			VarName.pop();
		}
	else if(!strcmp(lexer->YYText(), "BOOLEAN"))
		while(!VarName.empty()){
			DeclaredVariables.insert(pair<string, DATA_TYPE>(VarName.top(), BOOLEAN));
			cout << VarName.top() <<":\t.quad 0"<<endl;
			VarName.pop();
		}
	else
		Error("Undeffined type");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
DATA_TYPE Expression(void){
	OPREL oprel;
	DATA_TYPE DataType=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		if(SimpleExpression()!=DataType)
			Error("type different");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		DataType=BOOLEAN;
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
	}
return DataType;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if(DeclaredVariables[variable]!=Expression())
		Error("pas meme type de variable");
	cout << "\tpop "<<variable<<endl;
}
// WhileStatement := "WHILE" Expression DO Statement
void Statement(void);
void WhileStatement(void){
	unsigned long Num=++TagNumber;
	cout << "WHILE"<<Num<<":";
	current=(TOKEN) lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("type BOOLEAN attendu");
	cout<<"\tpop %rax\n";
	cout<<"\tcmpq $0,%rax\n";
	cout<<"\tje ENDWHILE"<<Num<<endl;
	if(strcmp(lexer->YYText(),"DO"))
		Error("KEYWORD 'DO' attendu");
	current=(TOKEN) lexer->yylex();
      	Statement();
	cout <<"\tjmp WHILE"<<Num<<"\n";
	cout <<"ENDWHILE"<<Num<<":\n";
}
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	unsigned long Num=++TagNumber;
	if(current!=KEYWORD || strcmp(lexer->YYText(),"IF"))
		Error("KEYWORD 'IF' attendu");
	current=(TOKEN) lexer->yylex();
	if(Expression()!=BOOLEAN)
		Error("type BOOLEAN atendu") ;
	cout<<"\tpop %rax"<<endl;
	cout<<"\tcmpq $0,%rax"<<endl;
	cout<<"\tje NEXT"<<Num<<endl;
	if(current!=KEYWORD || strcmp(lexer->YYText(),"THEN"))
		Error("KEYWORD 'THEN' attendu");
	current=(TOKEN) lexer->yylex();
	Statement();
	cout<<"NEXT"<<Num<<":"<<endl;
	if(current == KEYWORD && !strcmp(lexer->YYText(),"ELSE")){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
}
// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
void ForStatement(void){
	string gVariable;
	unsigned long Num=++TagNumber; 
	if(current!=KEYWORD || strcmp(lexer->YYText(),"FOR"))	
		Error("KEYWORD 'FOR' attendu");
	current=(TOKEN) lexer->yylex();
	gVariable = lexer->YYText();
	AssignementStatement();
	if(current!=KEYWORD && !strcmp(lexer->YYText(),"TO")){
		current=(TOKEN) lexer->yylex();
		cout <<"FOR"<<Num<<":"<<endl;
		Expression();
		if(current!=KEYWORD || strcmp(lexer->YYText(),"DO"))
        	        Error("KEYWORD 'DO' attendu");
		current=(TOKEN) lexer->yylex();
		cout<<"\tpop %rax"<<endl;
		cout<<"\tmovq "<<gVariable<<",%rbx"<<endl;
		cout<<"\tcmpq %rax,%rbx"<<endl;
		cout<<"\tja SUITE"<<Num<<endl;
		Statement();
		cout <<"\taddq $1,"<<gVariable<<endl;
		cout<<"\tjmp FOR"<<Num<<endl;
		cout<<"SUITE"<<Num<<":"<<endl;
	}else if(current==KEYWORD && !strcmp(lexer->YYText(),"DOWNTO")){
		current=(TOKEN) lexer->yylex();
		cout <<"FOR"<<Num<<":"<<endl;
		Expression();
		if(current!=KEYWORD || strcmp(lexer->YYText(),"DO"))
                	Error("KEYWORD 'DO' attendu");
		current=(TOKEN) lexer->yylex();
		cout<<"\tpop %rax"<<endl;
		cout<<"\tmovq "<<gVariable<<",%rbx"<<endl;
		cout<<"\tcmpq %rbx,%rax"<<endl;
		cout<<"\tjg SUITE"<<Num<<endl;
		Statement();
		cout <<"\tsubq $1,"<<gVariable<<endl;
		cout<<"\tjmp FOR"<<Num<<endl;
		cout<<"SUITE"<<Num<<":"<<endl;
	}else
		Error("KEYWORD 'TO ou DOWNTO' attendu");

}
//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	unsigned long var = ++TagNumber;
	if(current!=KEYWORD || strcmp(lexer->YYText(),"BEGIN"))
		Error("KEYWORD 'BEGIN' attendu");
	do{
		current=(TOKEN) lexer->yylex();
		Statement();
	}while(current==SEMICOLON);
	if(current!=KEYWORD || strcmp(lexer->YYText(),"END")){
		Error("KEYWORD 'END' attendu");
	}
	current=(TOKEN) lexer->yylex();
}
//DisplayStatement
void DisplayStatement(void){
	unsigned long tag = ++TagNumber;
	if(current!=KEYWORD || strcmp(lexer->YYText(),"DISPLAY"))
		Error("keyword DISPLAY attendu");
	current=(TOKEN) lexer->yylex();
	DATA_TYPE type=Expression();
	switch(type){
	case INTEGER:

		cout <<"\tpop %rdx"<<endl;
		cout <<"\tmovq $FormatString1, %rsi"<<endl;
		cout <<"\tmovl $1, %edi"<<endl;
		cout <<"\tmovl $0, %eax"<<endl;
		cout <<"\tcall __printf_chk@PLT"<<endl;
		break;
	case DOUBLE:
		cout << "\tmovsd (%rsp), %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi"<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp"<<endl;
		break;
	case CHAR:
		cout<<"\tpop %rsi"<<endl;
		cout << "\tmovq $FormatString3, %rdi"<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
		break;
	case BOOLEAN:
		cout << "\tpop %rdx"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rdi"<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rdi"<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tcall	puts@PLT"<<endl;
		break;
	default:
		Error("Type de variable non reconnu");
	}

}
void Empty(void ){
}
void CaseLabelList(void ){
	do{
		current=(TOKEN) lexer->yylex();
		Factor();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq	%rbx, %rax"<<endl;
		cout << "\tpush %rbx"<<endl;
		cout << "\tje SUITE"<<TagNumber<<endl;
	}while(current==COMMA);
}
//CaseListEelement := CaseLabelList ":" Statement | Empty
void CaseListElement(void ){
        unsigned long tag=++TagNumber;
	CaseLabelList();
	if(current != COLON)
		Error("Caractere ':' attendu");
	current = (TOKEN) lexer->yylex();
	cout <<"\tjmp NEXT"<<tag<<endl;
	cout<<"SUITE"<<tag<<":"<<endl;
	if(current == ID || current ==KEYWORD)
		Statement();
	else
		Empty();
	cout << "NEXT"<<tag<<":"<<endl;
}
//CaseStatement := "CASE" Expression "OF" CaseListElement {; CaseListElement} "END"
void CaseStatement(void ){
	if(current != KEYWORD || strcmp(lexer->YYText(),"CASE"))
                Error("Mot cle 'CASE' attendu");
        current=(TOKEN) lexer->yylex();
	Expression();
        if(current != KEYWORD || strcmp(lexer->YYText(),"OF"))
                Error("Mot cle 'OF' attendu");
	do{
		CaseListElement();
	}while(current==SEMICOLON);
	if(current!=KEYWORD || strcmp(lexer->YYText(),"END"))
		Error("Mot cle 'END' attendu");
	current=(TOKEN) lexer->yylex();
}


// Statement := AssignementStatement | WhileStatement | IfStatement
void Statement(void){
  if(current == ID){
    AssignementStatement();
  }else{
    if(current == KEYWORD){
      	if(!strcmp(lexer->YYText(),"WHILE")){
		WhileStatement();
      	}else if(!strcmp(lexer->YYText(),"IF")){
		IfStatement();
      	}else if(!strcmp(lexer->YYText(),"FOR")){
		ForStatement();
	}else if(!strcmp(lexer->YYText(),"BEGIN")){
		BlockStatement();
	}else if(!strcmp(lexer->YYText(),"DISPLAY")){
		DisplayStatement();
	}else if(!strcmp(lexer->YYText(),"CASE")){
		CaseStatement();
	}else{
	    	Error("KEYWORD not defined");
    	}
    }
   }
}

// StatementPart := Statement {";" Statemenr} "."
void StatementPart(void){
	cout << "\t.text"<<endl;
	cout << "FormatString1:    .string \"%llu\\n\""<<endl;
	cout << "FormatString2:\t.string \"%lf\\n\""<<endl;
	cout << "FormatString3:\t.string \"%c\\n\""<<endl;
	cout << "TrueString:\t.string \"TRUE\""<<endl; 
	cout << "FalseString:\t.string \"FALSE\""<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && !strcmp("VAR",lexer->YYText()))
		VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





